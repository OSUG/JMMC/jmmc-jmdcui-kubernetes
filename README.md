# jmmc-jmdcui-kubernetes

Deployement requires a dedicated ini file so we connect to the postgres server using a login/password info.

This secret is given to the application using a configmap. 

Please copy a pruduction model template from the application code source, update approriate field(s) and create the configmap.

Following step should help to reach a correct deployement:

- kubectl create configmap jmdc-ini --from-file=docker.ini=../jmmc-jmdcui-docker/jmdc-ui/production.ini
- kubectl edit configmap jmdc-ini # and edit sqlalchemy.url with postgresql://jmmc_jmdc:ABCDE%%toto@osug-postgres.u-ga.fr:5432/jmdc?sslmode=require 
- kubectl apply -f jmdcui.yml

Please add in your /etc/hosts the proper IP for the host name of your ingress to test without using main jmmc frontend onto the K8S OSUG clusters.


